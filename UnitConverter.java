/**
 * Class base for GUI
 * @author Woramate Jumroonsilp
 *
 */
public class UnitConverter {
	/**
	 * Use for convert
	 * @param amt
	 * @param fromUnit
	 * @param toUnit
	 * @return
	 */
	public double convert( double amt, Unit fromUnit, Unit toUnit ){ return fromUnit.convertTo(toUnit, amt); }
	/**
	 * return array of unit
	 * @return
	 */
	public Unit[] getUnit(){ return Length.values(); }
}