/**
 * Enum classm of length
 * @author Woramate Jumroonsilp
 *
 */
public enum Length implements Unit{

	METER("meter", 1.0),	
	KILOMETER("kilometer", 1000.0),
	CENTIMETER("centimeter", 0.01),
	MILE("mile", 1609.344),
	FOOT("foot", 0.30480),
	WA("wa", 2.0),
	RD("rd",92.75);
	
	private final String name;
	private final double value;
	
	/**
	 * constructor to create enum
	 * @param name
	 * @param value
	 */
	Length( String name, double value )
	{
		this.name = name;
		this.value = value;
	}

	/**
	 * Us to convert unit
	 */
	@Override
	public double convertTo(Unit other, double amt) {
		return this.value*amt/other.getValue();
	}

	/**
	 * return value of unit
	 */
	@Override
	public double getValue() {
		return this.value;
	}
	
}
