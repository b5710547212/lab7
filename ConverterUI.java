import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * UI for unitconverter
 * @author Woramate Jumroonsilp
 *
 */
public class ConverterUI extends JFrame {

	private UnitConverter unitconverter;

	private JPanel main;
	private JButton convertBtn, clrBtn;
	private JTextField inputF;
	private JTextField outputField;
	private JLabel equal = new JLabel("=");
	private JComboBox inputBox, outputBox;

	/**
	 * Constructor for gui
	 * @param uc
	 */
	public ConverterUI( UnitConverter uc )
	{
		this.unitconverter = uc;
		initComponent();
		this.setTitle("Simple Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.pack();
	}

	/**
	 * for initial components
	 */
	public void initComponent()
	{
		main = new JPanel();
		main.setLayout(new FlowLayout());//new BoxLayout(main,BoxLayout.X_AXIS));

		inputF = new JTextField(10);
		outputField = new JTextField(10);
		outputField.setEditable(false);
		
		inputBox = new JComboBox();
		outputBox = new JComboBox();

		convertBtn = new JButton("CONVERT!!");
		clrBtn = new JButton("Clear");

		Unit[] u = this.unitconverter.getUnit();
		for( Unit a : u )
		{
			inputBox.addItem(a);
			outputBox.addItem(a);
		}

		convertBtn.addActionListener(new ConvertButtonListener());
		clrBtn.addActionListener(new ClrButtonListener());
		inputF.addActionListener(new ConvertButtonListener());

		main.add(new JLabel());
		main.add(inputF);//Input field
		main.add(inputBox);//input unit
		main.add(equal);//equal sign
		main.add(outputField);//Output field
		main.add(outputBox);//output unit
		main.add(convertBtn);
		main.add(clrBtn);		

		super.add(main);
	}

	public void run()
	{
		this.setVisible(true);
	}
	public static void main(String[] args)
	{
		UnitConverter UC = new UnitConverter();
		ConverterUI converter = new ConverterUI(UC);
		converter.run();
	}

	class ConvertButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			try
			{
				double input = Double.parseDouble(inputF.getText());
				if(input > 0)
				{
					outputField.setText(unitconverter.convert(input,((Unit)inputBox.getSelectedItem()),((Unit)outputBox.getSelectedItem()))+"");
				}
				else{
					JOptionPane.showMessageDialog(null,"Number not more than 0","Error",JOptionPane.CLOSED_OPTION);
				}
			}catch(NumberFormatException e1)
			{
				JOptionPane.showMessageDialog(null,"NaN","Error",JOptionPane.CLOSED_OPTION);
			}
		}
	}
	class ClrButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			inputF.setText(null);
			outputField.setText(null);
		}
	}
}
