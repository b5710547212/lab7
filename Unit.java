/**
 * Interface of unit
 * @author Woramate Jumroonsilp
 *
 */
public interface Unit {

	/**
	 * Us to convert unit
	 */
	public double convertTo( Unit other, double amt);
	/**
	 * return value of unit
	 */
	public double getValue();
	public String toString();
	
}
